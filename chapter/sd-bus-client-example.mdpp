# Using the Systemd D-Bus API with **sd-bus**

**Note:** The files for this example can be found in the GitLab repository for the book
[^book-repository] in the directory **examples/example-sd-bus-client**.

The real power of the systemd D-Bus api can only be realized by using the api programmatically.
There are D-Bus bindings available for many different programming languages, a list of D-Bus
libraries can be found on freedesktop.org [^dbus-bindings].


## Calling D-Bus Methods

Because I want to focus the tutorial on systemd and systemd provides its own D-Bus C library
**sd-bus**, I will use this library here to develop a sample application. Lennart Poettering
described the library on his blog [^pid-eins-sd-bus] and I use his example code a basis for my
example here.

The first example just starts the service unit **python-sample-1.service** which I created in
a previous example. The code for the sample client follows below.

!INCLUDECODE "examples/example-sd-bus-client/sample-sd-bus-client.cpp" (C++)

As the server we implemented in a previous example, the client first connects to the
corresponding bus, in this case the system bus, using **sd_bus_default_system()**. After
connecting successfully, the client calls a method on the bus using **sd_bus_call_method()**,
passing the following arguments.

 - **bus**

    The pointer to the D-Bus
    
 - **destination**
 
    The service name the service registered on the bus.
    
 - **path**
 
    The path of the object to be called.
    
 - **interface**
 
    The interface to be called.
    
 - **member**
 
    The method to be called.
    
 - **ret_error**
 
    The D-Bus error to be called.
    
 - **reply**
 
    A pointer that will contain the reply message.
    
 - **types**
 
    The signature of the method in the D-Bus signature description.
    
 - **...**
 
    The variables to be used as parameters for the method call.

When the client calls the method using D-Bus, systemd creates a systemd job that in turn starts
the requested application. Running the client yields the following output on the console.

```
Queued service job as: /org/freedesktop/systemd1/job/55902frank@wotan:~/Documents/Development/systemd-by-example/cmake-build-debug/examples/
```

Checking on the status of the call using `systemctl status python-sample-1.service` yields the
following output and indicates that starting the service was successful.

```
● python-sample-1.service - The sample service
   Loaded: loaded (/etc/systemd/system/python-sample-1.service; static; vendor preset: enabled)
   Active: active (running) since Mon 2018-10-08 23:45:02 EDT; 12h ago
 Main PID: 7707 (python3)
    Tasks: 1 (limit: 4915)
   CGroup: /system.slice/python-sample-1.service
           └─7707 /usr/bin/python3 /usr/local/bin/sample_service_1.py

Oct 08 23:45:02 wotan systemd[1]: Started The sample service.
```


## Listening to D-Bus Signals

The sample client allows us to call the systemd D-Bus api and start a systemd service unit. In the
next example I will demonstrate how to listen to signals from the systemd D-Bus service to
determine when the service unit changes status.

The client listening to the signal has the following source code.

!INCLUDECODE "examples/example-sd-bus-client/sample-sd-bus-signal-listening.cpp" (C++)

Because the client has to wait for the signal to be emitted, it has to run in a loop. I used the
same code as in the event loop example to create and set up the event loop. The code is exactly the
same as before so I decided to not describe it here again.

After the client created the event loop, it registers the signal handler with systemd using the
function **sd_bus_match_signal()**. The function takes the following parameters.

 - **sd_bus**
 
    The service bus the client is connected to.
    
 - **return**
 
    The **sd_bus_slot**. The slot is used to manage the lifetime of the signal handler. If the
    lifetime should be the same as the bus, it is possible to pass a null pointer and **sd-bus**
    manages the lifetime.
    
 - **sender**
 
    The sender interface the client is listening for. The sender interface is
    "org.freedesktop.systemd1".
    
 - **path**

    The path the client is listening for. I am listening to the path of the specific serivce unit
    **python-sample-1.service**. Note the the path has to be escaped. The '-' is escaped with
    '_2d' and '.' is escaped with '_2e'.

 - **interface**
 
    The interface the client is listening for. In the example the interface is the standard
    D-Bus interface **org.freedesktop.DBus.Properties**.

 - **member**
 
    The signal the client is listening for. In the example the signal is *PropertiesChanged*.
    
 - **callback**
 
    The callback function to be called when the signal is received. The callback in the example
    is the function **signalCallback()** which handles the message received. The callback will
    be described in detail in the following section.
    
 - **userdata**
 
    A pointer to user data **sd-bus** will pass in the callback when it calls the callback.
    
    
### The Callback Function

When **sd-bus** retrieves the signal on the bus, it calls the callback function registered using
the function **sd_bus_match_signal()**. The callback function takes the following parameters.

 - **message**

    The message the signal sends.
 
 - **userData**
 
    A pointer of type *void* which can be registered when registering the callback function.
    **Sd-bus** will pass the pointer when it receives the signal and calls the callback function.
    
 - **returnError**
 
    An error that can be returned.

The callback in the example receives the signal *PropertiesChanged*. The signal emits a message
with the D-Bus signature `sa{sv}as`, showing that the messages has a value of type *string*,
followed by an array with a dictionary with a key of type *string* and a value of type *variant*,
followed by an array of type *string*. Further information about the D-Bus type system can be
found in teh D-Bus Specification [^dbus-specification].


#### Parsing the Message

The callback function parses the message and prints the message content to the console. The
signature only describes the types contained in the message, not the meaning. The standard D-Bus
interface **org.freedesktop.DBus.Properties** is described in the [^dbus-specification] and the
content of the signal in detail are as follows.

 - **interface_name** of type *string*
 
 - **changed_properties** a dictionary with key of type *string* and a value of type *variant*

 - **invalidated_properties** an array of *string*

I only implemented reading of the **interface_name** and the **changed_properties** in the
example because the implementation of reading the **invalidated_properties** can be done along
the lines of the **changed_properties** and would not add additional information.

First the service reads the **interface_name** which is either **org.freedesktop.systemd1.Unit**
or **org.freedesktop.systemd1.Service**, because the unit I am listening for changes in is a
service unit and implements these two interfaces. Because the **interface_name** is a simple
*string* it can be read with just on call to the function **sd_bus_message_read_basic** which
can be used to read a single value of a given type. The type can be any of the basic types
described in the systemd specification [^dbus-specification]. The **interface_name** is a
*string* and I pass *'s'* as type parameter. The function takes the message and a pointer to
store the value in addition to the type parameter. It also returns an error code in case
something goes wrong. The full call looks like this
`sd_bus_message_read_basic(message, 's', &interfaceName)`.

Reading an attribute with **sd_bus_message_read()** also moves the current position in the
message along so that we are now ready to read the next part of the message.

Reading the **changed_properties** is more complicated than reading a single attribute because
the **changed_properties** is an array of dictionaries. To access the array to access container
in the message. Container are arrays, structs, variants or dict_entries as
described in the D-Bus Specification [^dbus-specification]. To access a container, we have to
enter it using the function **sd_bus_message_enter_container()** and exit it using message
**sd_bus_message_exit_container()**. Depending on the type of the container the function
**sd_bus_enter_container()** behaves differently based on the type of the container.

 - Entering a container of type *array*

    Calling **sd_bus_message_enter_container()** if the current position in the message is at
    the beginning of an array enters the array and move the current position to the beginning of
    that array element.
    Simple elements in the array can be loaded by calling **sd_bus_message_read_basic()** and
    container in the array can be entered by calling **sd_bus_message_container_enter()**.
    Calling **sd_bus_message_container_enter()** for the elements in the array will open one
    element in the array after the other, returning an error code < 0 if there are no more
    elements in the array left.

 - Entering a container of type *dictionary*
 
    Entering a container of type dictionary by calling **sd_bus_enter_container()** allows
    reading the contents of the dictionary. After the contents have been read, the container can
    be exited using **sd_bus_exit_container()**. 

 - Entering a container of type *variant*
 
    Entering a container of type variant allows accessing the contained value. Because the value
    can be of any single complete type, the type has to be determined before it can be entered.
    The type in the variant can be determined by calling **sd_bus_message_peek_type()** which
    returns a string describing the type of the variant. Once the type has been determined, the
    variant can be entered by calling **sd_bus_message_container_enter**. Once all desired
    values are read, the variant can be exited by calling **sd_bus_message_exit_container**.
    
 - Entering a container of type *struct*
 
    TODO!!! How to access a struct correctly?

The function to enter a container **sd_bus_message_enter_container()** takes three parameters,
the message, the type of the container and the type of its contents. The type of the contents
is a type string. The type of the container can be selected by one of the following constants.

 - SD_BUS_TYPE_ARRAY
 - SD_BUS_TYPE_DICT_ENTRY
 - SD_BUS_TYPE_VARIANT
 - SD_BUS_TYPE_STRUCT

Therefore to read the **changed_properties** with type *a{sv}*, the client first enters the array
by calling `sd_bus_message_enter_container(message, SD_BUS_TYPE_ARRAY, {sv})`. It then calls
`sd_bus_message_enter_container(message, SD_BUS_TYPE_DICT_ENTRY, "sv")` in a while loop to
enter each dictionary element in the array one after the other. Once the dictionary container
was entered, the client can first read the dictionary key of type string by simply calling
`sd_bus_message_read_basic(message, 's', &attributeName)` which retrieves an attribute of type
*string* from the message, lets attribute name point on to the string and move the current
position in the message to the next attribute. Now that the attribute key was read, the client
can read the attribute value stored in the variant.

The variant can store attributes of any single complete type but I will only read the attribute
types of *'u'* for *uni32_t* and the attribute type of *'s'* for *string* which are used by the
attributes *MainPID* and *ActiveState* respectively.

To read the correct data type the client has to first decide which type the variant holds. It
does that by calling `sd_bus_message_peek_type(message, nullptr, &contents)` which stores the
type description string of the attribute at the current position of the message in the pointer
*contents*. With this information the client calls
`sd_bus_message_enter_container(message, SD_BUS_TYPE_VARIANT, contents)` to enter the variant
with the type description retrieved previously.

It then uses the type description string to determine if it has to read a *string* or a
*uint32_t* based on the type string an reads the value of the appropriate type by calling
`sd_bus_message_read_basic(message, contents[0], &unsignedIntValue)` or
`sd_bus_message_read_basic(message, contents[0], &stringAttribute)`. It then exits the variant by
calling `sd_bus_message_exit_container(message)` and the continues with the next element in the
array.

Starting the client and then stopping the unit **python-sample-1.service** by calling
`sudo systemctl stop python-sample-1` yields the following output.

```
Read attribute with name MainPID with type u
Attribute value 15412
Received signal for interface org.freedesktop.systemd1.Service
Read attribute with name ActiveState with type s
Attribute value deactivating
Received signal for interface org.freedesktop.systemd1.Unit
Read attribute with name MainPID with type u
Attribute value 0
Received signal for interface org.freedesktop.systemd1.Service
Read attribute with name ActiveState with type s
Attribute value inactive
Received signal for interface org.freedesktop.systemd1.Unit
```
