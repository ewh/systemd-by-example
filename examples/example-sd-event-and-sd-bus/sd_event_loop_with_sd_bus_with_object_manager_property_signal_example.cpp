#include <stdexcept>
#include <iostream>
#include <sstream>
#include <vector>
#include <memory>

#include <signal.h>

#include <systemd/sd-event.h>
#include <systemd/sd-bus.h>


using namespace std;

sd_event *createEventLoop() {
    int errorCode = 0;

    cout << "Creating event loop" << "\n";

    sd_event *event = nullptr;
    errorCode = sd_event_default(&event);
    if (errorCode < 0) {
        throw runtime_error("Could not create default event loop");
    }

    return event;
}

void blockSignals() {
    cout << "Clocking signals" << "\n";
    sigset_t signalSet;
    if (sigemptyset(&signalSet) < 0 ||
        sigaddset(&signalSet, SIGTERM) < 0 ||
        sigaddset(&signalSet, SIGINT))
    {
        throw runtime_error("Could not setup signal set");
    }

    if (sigprocmask(SIG_BLOCK, &signalSet, nullptr) < 0) {
        throw runtime_error("Could not block signals");
    }
}

void addSignalHandler(sd_event *event) {
    int errorCode = 0;

    cout << "Adding signal handler" << "\n";
    errorCode = sd_event_add_signal(event, nullptr, SIGTERM, nullptr, nullptr);
    if (errorCode < 0) {
        throw runtime_error("Could not add signal handler for SIGTERM to event loop");
    }

    errorCode = sd_event_add_signal(event, nullptr, SIGINT, nullptr, nullptr);
    if (errorCode < 0) {
        throw runtime_error("Could not add signal handler for SIGINT to event loop");
    }
}

struct CounterData {
    uint32_t count = 0;
};

int methodIncrement(sd_bus_message *message, void *userData, sd_bus_error *error) {
    uint32_t parameter = 0;
    sd_bus_message_read(message, "u", &parameter);
    auto counterData = static_cast<CounterData *>(userData);
    counterData->count += parameter;
    sd_bus *sdBus = sd_bus_message_get_bus(message);
    const char *path = sd_bus_message_get_path(message);
    const char*interface = sd_bus_message_get_interface(message);
    sd_bus_emit_properties_changed(sdBus, path, interface, "Count", nullptr);
    return sd_bus_reply_method_return(message, "u", counterData->count);
}

int propertyCounterGet(
        sd_bus *bus, const char *path, const char *interface, const char *property,
        sd_bus_message *reply, void *userData, sd_bus_error *error)
{
    auto counterData = static_cast<CounterData *>(userData);
    return sd_bus_message_append(reply, "u", counterData->count);
}

int propertyCounterSet(
        sd_bus *bus, const char *path, const char *interface, const char *property,
        sd_bus_message *message, void *userData, sd_bus_error *error)
{
    uint32_t parameter = 0;
    sd_bus_message_read(message, "u", &parameter);
    auto counterData = static_cast<CounterData *>(userData);
    counterData->count = parameter;
    sd_bus_emit_properties_changed(bus, path, interface, "Count", nullptr);
    return 1;
}

static const sd_bus_vtable counter_vtable[] = {
    SD_BUS_VTABLE_START(0),
    SD_BUS_WRITABLE_PROPERTY(
        "Count", "u", propertyCounterGet,
        propertyCounterSet, 0, SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
    SD_BUS_METHOD("IncrementBy", "u", "u", methodIncrement, SD_BUS_VTABLE_UNPRIVILEGED),
    SD_BUS_VTABLE_END
};

void addCounterObjectToService(sd_bus *sdBus, const uint32_t id, CounterData *counterData) {
    ostringstream pathOss;
    pathOss << "/net/franksreich/counter1/" << id;
    int errorCode = 0;
    errorCode = sd_bus_add_object_vtable(
            sdBus,
            nullptr,
            pathOss.str().c_str(),
            "net.franksreich.Counter",
            counter_vtable,
            counterData);
    if (errorCode < 0 ) {
        ostringstream oss;
        oss << "Could not add object v-table, " << strerror(-errorCode);
        throw runtime_error(oss.str());
    }
}

struct ApplicationData {
    sd_bus *sdBus = nullptr;
    vector<unique_ptr<CounterData>> counters;
};

int methodAddCounter(sd_bus_message *message, void *userData, sd_bus_error *error) {
    uint32_t parameter = 0;
    sd_bus_message_read(message, "u", &parameter);
    auto data = static_cast<ApplicationData *>(userData);
    data->counters.push_back(make_unique<CounterData>());
    data->counters[data->counters.size() - 1]->count = parameter;
    addCounterObjectToService(
        data->sdBus,
        (uint32_t) data->counters.size() - 1,
        data->counters[data->counters.size() - 1].get());
    ostringstream pathOss;
    pathOss << "/net/franksreich/counter1/" << data->counters.size() - 1;
    sd_bus_emit_object_added(data->sdBus, pathOss.str().c_str());
    return sd_bus_reply_method_return(message, "u", data->counters.size() - 1);
}

static const sd_bus_vtable manager_vtable[] = {
    SD_BUS_VTABLE_START(0),
    SD_BUS_METHOD("AddCounter", "u", "u", methodAddCounter, SD_BUS_VTABLE_UNPRIVILEGED),
    SD_BUS_VTABLE_END
};

void runEventLoop(sd_event *event) {
    int errorCode = 0;

    cout << "Starting event loop" << "\n";
    errorCode = sd_event_loop(event);
    if (errorCode < 0) {
        throw runtime_error("Error while running event loop");
    }
}

sd_bus *connectToSystemBus() {
    sd_bus *sdBus = nullptr;
    int errorCode = 0;
    errorCode = sd_bus_default_user(&sdBus);
    if (errorCode < 0) {
        throw runtime_error("Could not connect to system bus");
    }
    return sdBus;
}

void addManagerObjectToService(sd_bus *sdBus, ApplicationData *applicationData) {
    int errorCode = 0;
    errorCode = sd_bus_add_object_vtable(
            sdBus,
            nullptr,
            "/net/franksreich/counter1",
            "net.franksreich.Manager",
            manager_vtable,
            applicationData);
    if (errorCode < 0 ) {
        ostringstream oss;
        oss << "Could not add object v-table, " << strerror(-errorCode);
        throw runtime_error(oss.str());
    }
}

void requestServiceName(sd_bus *sdBus, string name) {
    int errorCode = 0;
    errorCode = sd_bus_request_name(sdBus, name.c_str(), 0);
    if (errorCode < 0) {
        ostringstream oss;
        oss << "Could not request name, " << strerror(-errorCode);
        throw runtime_error(oss.str());
    }
}

void attachBusHandlingToEventLoop(sd_bus *sdBus, sd_event *event) {
    int errorCode = 0;
    errorCode = sd_bus_attach_event(sdBus, event, 0);
    if (errorCode < 0) {
        throw runtime_error("Could not add sd-bus handling to event bus");
    }
}

int main(int argc, char *argv[]) {
    sd_event *event = nullptr;
    ApplicationData applicationData;

    try {
        event = createEventLoop();
        blockSignals();
        addSignalHandler(event);
        applicationData.sdBus = connectToSystemBus();
        CounterData counterData;
        sd_bus_add_object_manager(applicationData.sdBus, nullptr, "/net/franksreich/counter1");
        addManagerObjectToService(applicationData.sdBus, &applicationData);
        requestServiceName(applicationData.sdBus, "net.franksreich.counter1");
        attachBusHandlingToEventLoop(applicationData.sdBus, event);
        runEventLoop(event);
    } catch (runtime_error &error) {
        cerr << error.what();
    }

    event = sd_event_unref(event);
    applicationData.sdBus = sd_bus_unref(applicationData.sdBus);
}
