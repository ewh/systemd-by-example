#include <stdexcept>
#include <iostream>
#include <sstream>

#include <signal.h>

#include <systemd/sd-event.h>
#include <systemd/sd-bus.h>


using namespace std;

sd_event *createEventLoop() {
    int errorCode = 0;

    cout << "Creating event loop" << "\n";

    sd_event *event = nullptr;
    errorCode = sd_event_default(&event);
    if (errorCode < 0) {
        throw runtime_error("Could not create default event loop");
    }

    return event;
}

void blockSignals() {
    cout << "Clocking signals" << "\n";
    sigset_t signalSet;
    if (sigemptyset(&signalSet) < 0 ||
        sigaddset(&signalSet, SIGTERM) < 0 ||
        sigaddset(&signalSet, SIGINT))
    {
        throw runtime_error("Could not setup signal set");
    }

    if (sigprocmask(SIG_BLOCK, &signalSet, nullptr) < 0) {
        throw runtime_error("Could not block signals");
    }
}

void addSignalHandler(sd_event *event) {
    int errorCode = 0;

    cout << "Adding signal handler" << "\n";
    errorCode = sd_event_add_signal(event, nullptr, SIGTERM, nullptr, nullptr);
    if (errorCode < 0) {
        throw runtime_error("Could not add signal handler for SIGTERM to event loop");
    }

    errorCode = sd_event_add_signal(event, nullptr, SIGINT, nullptr, nullptr);
    if (errorCode < 0) {
        throw runtime_error("Could not add signal handler for SIGINT to event loop");
    }
}

struct CounterData {
    uint32_t count = 0;
};

int methodIncrement(sd_bus_message *message, void *userData, sd_bus_error *error) {
    int errorCode = 0;
    uint32_t parameter = 0;
    errorCode = sd_bus_message_read(message, "u", &parameter);
    auto counterData = static_cast<CounterData *>(userData);
    counterData->count += parameter;
    return sd_bus_reply_method_return(message, "u", counterData->count);
}

int propertyCounterGet(
        sd_bus *bus, const char *path, const char *interface, const char *property,
        sd_bus_message *reply, void *userData, sd_bus_error *error)
{
    auto counterData = static_cast<CounterData *>(userData);
    return sd_bus_message_append(reply, "u", counterData->count);
}

int propertyCounterSet(
        sd_bus *bus, const char *path, const char *interface, const char *property,
        sd_bus_message *message, void *userData, sd_bus_error *error)
{
    int errorCode = 0;
    uint32_t parameter = 0;
    errorCode = sd_bus_message_read(message, "u", &parameter);
    auto counterData = static_cast<CounterData *>(userData);
    counterData->count = parameter;
}

static const sd_bus_vtable counter_vtable[] = {
   SD_BUS_VTABLE_START(0),
   SD_BUS_WRITABLE_PROPERTY("Count", "u", propertyCounterGet, propertyCounterSet, 0, 0),
   SD_BUS_METHOD("IncrementBy", "u", "u", methodIncrement, SD_BUS_VTABLE_UNPRIVILEGED),
   SD_BUS_VTABLE_END
};

void runEventLoop(sd_event *event) {
    int errorCode = 0;

    cout << "Starting event loop" << "\n";
    errorCode = sd_event_loop(event);
    if (errorCode < 0) {
        throw runtime_error("Error while running event loop");
    }
}

int main(int argc, char *argv[]) {
    sd_event *event = nullptr;
    sd_bus *sdBus = nullptr;

    try {
        event = createEventLoop();
        blockSignals();
        addSignalHandler(event);

        int errorCode = 0;
        errorCode = sd_bus_default_user(&sdBus);
        if (errorCode < 0) {
            throw runtime_error("Could not connect to system bus");
        }

        CounterData counterData;
        errorCode = sd_bus_add_object_vtable(
            sdBus,
            nullptr,
            "/net/franksreich/Counter",
            "net.franksreich.Counter",
            counter_vtable,
            &counterData);
        if (errorCode < 0 ) {
            ostringstream oss;
            oss << "Could not add object v-table, " << strerror(-errorCode);
            throw runtime_error(oss.str());
        }

        errorCode = sd_bus_request_name(sdBus, "net.franksreich.Counter", 0);
        if (errorCode < 0) {
            ostringstream oss;
            oss << "Could not request name, " << strerror(-errorCode);
            throw runtime_error(oss.str());
        }

        errorCode = sd_bus_attach_event(sdBus, event, 0);
        if (errorCode < 0) {
            throw runtime_error("Could not add sd-bus handling to event bus");
        }

        runEventLoop(event);
    } catch (runtime_error &error) {
        cerr << error.what();
    }

    event = sd_event_unref(event);
    sdBus = sd_bus_unref(sdBus);
}
