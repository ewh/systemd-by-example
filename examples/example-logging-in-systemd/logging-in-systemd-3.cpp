#include <iostream>

#include <systemd/sd-journal.h>


using namespace std;

int main(int argc, char *argv[]) {
    sd_journal_send(
            "MESSAGE=Simple log message",
            "PRIORITY=%i", LOG_NOTICE,
            "EXAMPLE=systemd",
            nullptr);

    sd_journal_send(
            "MESSAGE=Simple error message",
            "PRIORITY=%i", LOG_ERR,
            "EXAMPLE=systemd",
            nullptr);
}
