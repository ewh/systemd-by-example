#include <iostream>

#include <systemd/sd-journal.h>

using namespace std;

int main(int argc, char *argv[]) {
    cout << "<" << LOG_NOTICE << ">" << "Simple log message" << "\n";
    cerr << "<" << LOG_ERR << ">" <<"Simple error message" << "\n";
}
