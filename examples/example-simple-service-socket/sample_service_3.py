import systemd.daemon as daemon
import socket
import threading


def _create_socket(file_descriptor):
    if daemon.is_socket_unix(file_descriptor) > 0:
        return socket.fromfd(file_descriptor, socket.AF_UNIX, socket.SOCK_STREAM)
    elif daemon.is_socket_inet(file_descriptor, socket.AF_INET) > 0:
        return socket.fromfd(file_descriptor, socket.AF_INET, socket.SOCK_STREAM)
    elif daemon.is_socket_inet(file_descriptor, socket.AF_INET6) > 0:
        return socket.fromfd(file_descriptor, socket.AF_INET6, socket.SOCK_STREAM)
    else:
        return None


def _client_handler(connection):
    while True:
        data = connection[0].recv(4096)
        if not data:
            break
        print('Received from {}: {}'.format(connection[1], data))
        connection[0].send(data)


if __name__ == '__main__':
    socket_file_descriptors = daemon.listen_fds()
    socket_file_descriptor = socket_file_descriptors[0]
    server_socket = _create_socket(socket_file_descriptor)

    if server_socket is not None:
        while True:
            connection = server_socket.accept()
            handler_tread = threading.Thread(target=_client_handler, args=(connection,))
            handler_tread.start()
