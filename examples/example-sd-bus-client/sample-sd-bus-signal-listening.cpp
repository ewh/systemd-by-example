#include <iostream>
#include <sstream>
#include <stdexcept>

#include <systemd/sd-bus.h>
#include <systemd/sd-event.h>


using namespace std;

sd_event *createEventLoop() {
    int errorCode = 0;

    cout << "Creating event loop" << "\n";

    sd_event *event = nullptr;
    errorCode = sd_event_default(&event);
    if (errorCode < 0) {
        throw runtime_error("Could not create default event loop");
    }

    return event;
}

void blockSignals() {
    cout << "Clocking signals" << "\n";
    sigset_t signalSet;
    if (sigemptyset(&signalSet) < 0 ||
        sigaddset(&signalSet, SIGTERM) < 0 ||
        sigaddset(&signalSet, SIGINT))
    {
        throw runtime_error("Could not setup signal set");
    }

    if (sigprocmask(SIG_BLOCK, &signalSet, nullptr) < 0) {
        throw runtime_error("Could not block signals");
    }
}

void addSignalHandler(sd_event *event) {
    int errorCode = 0;

    cout << "Adding signal handler" << "\n";
    errorCode = sd_event_add_signal(event, nullptr, SIGTERM, nullptr, nullptr);
    if (errorCode < 0) {
        throw runtime_error("Could not add signal handler for SIGTERM to event loop");
    }

    errorCode = sd_event_add_signal(event, nullptr, SIGINT, nullptr, nullptr);
    if (errorCode < 0) {
        throw runtime_error("Could not add signal handler for SIGINT to event loop");
    }
}

void runEventLoop(sd_event *event) {
    int errorCode = 0;

    cout << "Starting event loop" << "\n";
    errorCode = sd_event_loop(event);
    if (errorCode < 0) {
        throw runtime_error("Error while running event loop");
    }
}

int signalCallback(sd_bus_message *message, void *userData, sd_bus_error *returnError) {
    int errorCode = 0;
    char *interfaceName = nullptr;
    errorCode = sd_bus_message_read_basic(message, 's', &interfaceName);
    if (errorCode < 0) {
        ostringstream errorMessage;
        errorMessage << "could not decode message, error: "
                     << strerror(-errorCode);
        throw runtime_error(errorMessage.str());
    }

    errorCode = sd_bus_message_enter_container(message, SD_BUS_TYPE_ARRAY, "{sv}");
    if (errorCode < 0) {
        ostringstream errorMessage;
        errorMessage << "could not decode message, error: "
                     << strerror(-errorCode);
        throw runtime_error(errorMessage.str());
    }

    while ((errorCode = sd_bus_message_enter_container(message, SD_BUS_TYPE_DICT_ENTRY, "sv")) > 0) {
        char *attributeName = nullptr;
        errorCode = sd_bus_message_read_basic(message, 's', &attributeName);
        if (errorCode < 0) {
            ostringstream errorMessage;
            errorMessage << "could not read attribute name, error: "
                         << strerror(-errorCode);
            throw runtime_error(errorMessage.str());
        }

        const char *contents = nullptr;
        errorCode = sd_bus_message_peek_type(message, nullptr, &contents);
        if (errorCode < 0) {
            ostringstream errorMessage;
            errorMessage << "could not read peek type, error: "
                         << strerror(-errorCode);
            throw runtime_error(errorMessage.str());
        }

        cout << "Read attribute with name " << attributeName
             << " with type " << contents << "\n";

        errorCode = sd_bus_message_enter_container(message, SD_BUS_TYPE_VARIANT, contents);
        if (errorCode < 0) {
            ostringstream errorMessage;
            errorMessage << "could not enter variant, error: "
                         << strerror(-errorCode);
            throw runtime_error(errorMessage.str());
        }

        if (string(contents) == "u") {
            uint32_t unsignedIntValue;

            errorCode = sd_bus_message_read_basic(message, contents[0], &unsignedIntValue);
            if (errorCode < 0) {
                ostringstream errorMessage;
                errorMessage << "could read uint32 parameter, error: "
                             << strerror(-errorCode);
                throw runtime_error(errorMessage.str());
            }

            cout << "Attribute value " << unsignedIntValue << "\n";
        } else if (string(contents) == "s") {
            const char *stringAttribute = nullptr;

            errorCode = sd_bus_message_read_basic(message, contents[0], &stringAttribute);
            if (errorCode < 0) {
                ostringstream errorMessage;
                errorMessage << "could read string parameter, error: "
                             << strerror(-errorCode);
                throw runtime_error(errorMessage.str());
            }

            cout << "Attribute value " << stringAttribute << "\n";
        }

        errorCode = sd_bus_message_exit_container(message);
        if (errorCode < 0) {
            ostringstream errorMessage;
            errorMessage << "could not exit container, error: "
                         << strerror(-errorCode);
            throw runtime_error(errorMessage.str());
        }
    }

    cout << "Received signal for interface " << interfaceName << "\n";
    return errorCode;
}

int main(int argc, char *argv[]) {
    sd_bus_error sdBusError = SD_BUS_ERROR_NULL;
    sd_bus_message *message = nullptr;
    sd_bus *sdBus = nullptr;
    sd_event *event = nullptr;
    char *path;
    int errorCode = 0;

    try {
        event = createEventLoop();
        blockSignals();
        addSignalHandler(event);

        errorCode = sd_bus_default_system(&sdBus);
        if (errorCode < 0) {
            ostringstream errorMessage;
            errorMessage << "Failed to connect to system bus, error: "
                         << strerror(-errorCode);
            throw runtime_error(errorMessage.str());
        }

        errorCode = sd_bus_match_signal(
                sdBus,
                nullptr,
                "org.freedesktop.systemd1",
                "/org/freedesktop/systemd1/unit/python_2dsample_2d1_2eservice",
                "org.freedesktop.DBus.Properties",
                "PropertiesChanged",
                signalCallback,
                nullptr);
        if (errorCode < 0) {
            ostringstream errorMessage;
            errorMessage << "Failed to match signal, error: "
                         << strerror(-errorCode);
            throw runtime_error(errorMessage.str());
        }

        errorCode = sd_bus_attach_event(sdBus, event, 0);
        if (errorCode < 0) {
            throw runtime_error("Could not add sd-bus handling to event bus");
        }

        runEventLoop(event);
    } catch (runtime_error &error) {
        cout << error.what() << "\n";
    }

    event = sd_event_unref(event);
    sd_bus_error_free(&sdBusError);
    sd_bus_message_unref(message);
    sd_bus_unref(sdBus);
}
