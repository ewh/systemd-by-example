#include <iostream>
#include <sstream>
#include <stdexcept>

#include <systemd/sd-bus.h>


using namespace std;

int main(int argc, char *argv[]) {
    sd_bus_error sdBusError = SD_BUS_ERROR_NULL;
    sd_bus_message *message = nullptr;
    sd_bus *sdBus = nullptr;
    char *path;
    int errorCode = 0;

    try {
        errorCode = sd_bus_default_system(&sdBus);
        if (errorCode < 0) {
            ostringstream errorMessage;
            errorMessage << "Failed to connect to system bus, error: "
                         << strerror(-errorCode);
            throw runtime_error(errorMessage.str());
        }

        errorCode = sd_bus_call_method(
                sdBus,
                "org.freedesktop.systemd1",
                "/org/freedesktop/systemd1",
                "org.freedesktop.systemd1.Manager",
                "StartUnit",
                &sdBusError,
                &message,
                "ss",
                "python-sample-1.service",
                "replace");
        if (errorCode < 0) {
            ostringstream errorMessage;
            errorMessage << "Failed to start python-sample-1.service, error: "
                         << strerror(-errorCode);
            throw runtime_error(errorMessage.str());
        }

        errorCode = sd_bus_message_read(message, "o", &path);
        if (errorCode < 0) {
            ostringstream errorMessage;
            errorMessage << "Failed to parse response message, error: "
                         << strerror(-errorCode);
            throw runtime_error(errorMessage.str());
        }

        cout << "Queued service job as: " << path;
    } catch (runtime_error &error) {
        cout << error.what() << "\n";
    }

    sd_bus_error_free(&sdBusError);
    sd_bus_message_unref(message);
    sd_bus_unref(sdBus);
}
